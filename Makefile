bin/dawn_arrival: $(shell nitls -M src/dawn_arrival.nit -m linux)
	mkdir -p bin/
	nitc src/dawn_arrival.nit -m linux -o $@

android: bin/dawn_arrival.apk
bin/dawn_arrival.apk: $(shell nitls -M src/android.nit) android/res/
	mkdir -p bin/
	nitc src/android.nit -o $@

clean:
	rm -rf bin/

check:
	nitunit .
