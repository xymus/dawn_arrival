# DAWN ARRIVAL

Hack 'n' slash game where you get rid of the monsters threatening to plunge your world into eternal darkness.

## Screenshots

![spawn](screenshots/play-spawn.png)

![forest map](screenshots/play-dungeon.png)

## Levels

There are 3 procedurally generated levels using two different algorithms:

### Forest

![forest map](screenshots/map-forest.png)

### Dungeon

![dungeon map](screenshots/map-dungeon.png)

### Island

![island map](screenshots/map-island.png)

## Compile, run and win

1. Install Nit and gamnit by following the guide at http://gamnit.org/#docs.

2. Compile the game with a simple call to `make` from the root of this repository.

3. Run with `bin/dawn_arrival`.

4. Reach the island through the dungeon to find the treasure and win!

## Art

logo, sunrise pixelart: noirlac.tumblr.com

music: https://soundcloud.com/sayzzn
