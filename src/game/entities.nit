import levels
import gamnit::textures
import gamnit::flat
import geometry

redef class Level
	var enemies = new Array[Enemy]

	var player: Player
end

redef class ActiveEntity

	fun top: Float do return center.y + 0.5
	fun bottom: Float do return center.y - 0.5
	fun left: Float do return center.x - 0.5
	fun right: Float do return center.x + 0.5

	var move_speed = 6.0

	fun die
	do
		if cpt_death < death_anim.length * 5 then
			if cpt_death % 5 == 0 then
				texture = death_anim[cpt_death/5]
				update_sprite
			end
			cpt_death += 1
		end
	end

	fun move (dt, dx, dy: Float)
	do
		center.x += move_speed*dx*dt
		center.y += move_speed*dy*dt
		if dx < 0.0 then
			position = ori_left
		else if dx > 0.0 then
			position = ori_right
		else
			if dy < 0.0 then
				position = ori_front
			else
				position = ori_back
			end
		end
	end

	fun hurt(x: Int) do life-=x
end

class Player
	super ActiveEntity

	var cpt: Int = 0
	var change_sprite: Bool = false
	var weapon: Weapon
	var attacking: Bool = false
	var cpt_attack: Int = 0


	init
	do
		level.entities.add self
		#hitbox = new Box[Float].ltwh(-16.5, 28.0, 33.0, 56.0)
		sprite.scale = 0.03125
		level.player = self
	end

	redef fun move(dx, dy, dt) do
		super
	end

	fun pickup(w: Weapon) do
		weapon = w
	end

	fun focused_enemy: nullable Enemy
	do
		var res = null
		if position == ori_left then
			for e in level.enemies do
				if level.get_tile(e.center.x.round.to_i, e.center.y.round.to_i) == level.get_tile(left.round.to_i, center.y.round.to_i) then
					res = e
				end
			end
		else if position == ori_right then
			for e in level.enemies do
				if level.get_tile(e.center.x.round.to_i, e.center.y.round.to_i) == level.get_tile(right.round.to_i, center.y.round.to_i) then
					res = e
				end
			end
		else if position == ori_back then
			for e in level.enemies do
				if level.get_tile(e.center.x.round.to_i, e.center.y.round.to_i) == level.get_tile(center.x.round.to_i, top.round.to_i) then
					res = e
				end
			end
		else
			for e in level.enemies do
				if level.get_tile(e.center.x.round.to_i, e.center.y.round.to_i) == level.get_tile(center.x.round.to_i, bottom.round.to_i) then
					res = e
				end
			end
		end
		return res
	end

	fun attack do
		attacking = true
		if cpt_attack < (weapon.nb_anim_attack-1) * 4 then
			cpt_attack+=1
			if cpt_attack % 4 == 0 then
				texture = weapon.attack_anim[position][cpt_attack/4]
				update_sprite
				var focused_enemy = focused_enemy
				if focused_enemy != null then
					focused_enemy.hurt(weapon.dmg)
				end
			end
		else
			cpt_attack = 0
			attacking = false
		end
	end
end

class Enemy
	super ActiveEntity

	var walk_anim: HashMap[Orientation, Array[Texture]]
	var attack_anim: HashMap[Orientation, Array[Texture]]
	var nb_anim_attack: Int = attack_anim[ori_left].length is lazy
	var dmg: Int
	var is_engaged: Bool = false
	var attacking: Bool = false
	var cpt_attack: Int = 0
	public var cpt_respawn: Float = 0.0

	init do
		level.entities.add self
		sprite.scale = 0.033
	end

	fun tick(dt: Float)
	do

	end

	fun tick_respawn(dt: Float): Float
	do
		cpt_respawn += dt
		return cpt_respawn
	end

	redef fun respawn(x)
	do
		super
		is_engaged = false
		cpt_respawn = 0.0
	end

	fun engage
	do
		if (center.x - level.player.center.x).abs < 3.0 and (center.y - level.player.center.y).abs < 3.0 then
			is_engaged = true
		end
	end

	fun disengage do is_engaged = false

	fun attack do
		attacking = true
		if cpt_attack < (nb_anim_attack-1)*6 then
			if cpt_attack%6 == 0 then
				texture = attack_anim[position][cpt_attack/6]
				update_sprite
				level.player.hurt(dmg)
			end
		else
			cpt_attack = -1
		end
		cpt_attack += 1
	end

	redef fun move(dt, dx, dy)
	do
		super
		attacking = false
	end

	fun face_player(direction: String, gap: Float)
	do
		if direction == "vert" then
			if gap > 0.0 then
				position = ori_front
			else
				position = ori_back
			end
		else if direction == "horiz" then
			if gap > 0.0 then
				position = ori_left
			else
				position = ori_right
			end
		end
	end
end

class PatternEnemy
	super Enemy

	var start: Tile
	var end_t: Tile
	var start_to_end: Bool = true
	private var cpt: Int = 0
	redef var move_speed = 4.0

	redef fun tick(dt)
	do
		if is_engaged and level.player.is_alive then
			var gap_x = center.x - level.player.center.x
			var gap_y = center.y - level.player.center.y
			if gap_x.abs < 0.5 then
				if gap_y < -0.7 then
					move(dt, 0.0, 0.8)
				else if gap_y > 0.7 then
					move(dt, 0.0, -0.8)
				else
					face_player("vert", gap_y)
					attack
				end
			else if gap_x.abs < 1.0 then
				if gap_y.abs < 0.03 then
					face_player("horiz", gap_x)
					attack
				else if gap_y < 0.0 then
					move(dt, 0.0, 0.8)
				else if gap_y > 0.0 then
					move(dt, 0.0, -0.8)
				end
			else if gap_x < 0.0 then
				move(dt, 0.8, 0.0)
			else if gap_x > 0.0 then
				move(dt, -0.8, 0.0)
			end
		else
			var cx = center.x.round
			var cy = center.y.round
			var dest_x
			var dest_y
			if start_to_end then
				dest_x = end_t.center.x.round
				dest_y = end_t.center.y.round
			else
				dest_x = start.center.x.round
				dest_y = start.center.y.round
			end

			if cx < dest_x then
				move(dt, 0.5, 0.0)
			else if cx > dest_x then
				move(dt, -0.5, 0.0)
			else if cy < dest_y then
				move(dt, 0.0, 0.5)
			else if cy > dest_y then
				move(dt, 0.0, -0.5)
			else
				start_to_end = not start_to_end
			end
		end
		if cpt%3==0 and not(attacking) then
			texture = walk_anim[position][cpt/3]
			update_sprite
		end
		cpt+=1
		if cpt == (walk_anim[position].length-1) * 3 then cpt = 0
	end

	redef fun respawn(x)
	do
		super
		center.x = start.center.x
		center.y = start.center.y
	end

	init
	do
		super
		level.enemies.add self
	end
end

class GuardEnemy
	super Enemy

	var start: Tile
	private var cpt: Int = 0

	redef fun engage
	do
		var player = level.player
		if player.is_alive then
			var gap_x = center.x - player.center.x
			var gap_y = center.y - player.center.y
			if position == ori_left then
				if player.center.x < center.x and player.center.x > center.x - 6.0 and gap_y < 0.5 then is_engaged = true
			else if position == ori_right then
				if player.center.x > center.x and player.center.x < center.x + 6.0 and gap_y < 0.5 then is_engaged = true
			else if position == ori_back then
				if player.center.y > center.y and player.center.y < center.y + 6.0 and gap_x < 0.5 then is_engaged = true
			else
				if player.center.y < center.y and player.center.y > center.y - 6.0 and gap_x < 0.5 then is_engaged = true
			end
		end
	end

	redef fun tick(dt)
	do
		var gap_x = center.x - level.player.center.x
		var gap_y = center.y - level.player.center.y
		if is_engaged and level.player.is_alive then
			if gap_x.abs < 0.5 then
				if gap_y < -0.7 then
					move(dt, 0.0, 0.8)
				else if gap_y > 0.7 then
					move(dt, 0.0, -0.8)
				else
					face_player("vert", gap_y)
					attack
				end
			else if gap_x.abs < 1.0 then
				if gap_y.abs < 0.03 then
					face_player("horiz", gap_x)
					attack
				else if gap_y < 0.0 then
					move(dt, 0.0, 0.8)
				else if gap_y > 0.0 then
					move(dt, 0.0, -0.8)
				end
			else if gap_x < 0.0 then
				move(dt, 0.8, 0.0)
			else if gap_x > 0.0 then
				move(dt, -0.8, 0.0)
			end
		else
			if center.x.round < start.center.x.round then
				move(dt, 0.5, 0.0)
			else if center.x.round > start.center.x.round then
				move(dt, -0.5, 0.0)
			else
				if center.y.round < start.center.y.round then
					move(dt, 0.0, 0.5)
				else if center.y.round > start.center.y.round then
					move(dt, 0.0, -0.5)
				end
			end

		end
		if cpt%3==0 and not(attacking) then
			if is_engaged or (center.x.round != start.center.x or center.y.round != start.center.y) then
				texture = walk_anim[position][cpt/3]
				update_sprite
			end
		end
		cpt+=1
		if cpt == (walk_anim[position].length-1) * 3 then
			cpt = 0
			if (not is_engaged) and center.x.round == start.center.x and center.y.round == start.center.y then
				if position == ori_left then
					position = ori_front
				else if position == ori_front then
					position = ori_right
				else if position == ori_right then
					position = ori_back
				else if position == ori_back then
					position = ori_left
				end
				texture = walk_anim[position][0]
				update_sprite
			end
		end
	end

	redef fun respawn(x)
	do
		super
		center.x = start.center.x
		center.y = start.center.y
	end

	init
	do
		super
		level.enemies.add self
	end
end

redef class CollidableTile

	# Named constructor
	#
	# Usage example:
	#
	# ~~~
	# var my_tile = new CollidableTile.from_size(0, 0, 16, 16, my_level)
	# ~~~
	#init from_size(left, top, width, height: Float, lvl: Level, t: Texture)
	#do
		#hitbox = new Box[Float].ltwh(left, top, width, height)
	#	level = lvl
	#	center = new Point3d[Float](left+(width/2.0),top-(height/2.0),1.0)
	#	init(center, level, t)
	#	sprite.scale = 0.05/width
	#end

	init
	do
		is_collidable = true
	end
end

redef class Weapon
	init from_tile(level: Level, texture: Texture, dmg: Int, spawn_tile: Tile, walk_anim, attack_anim: HashMap[Orientation, Array[Texture]], nb_anim_attack: Int)
	do
				center = spawn_tile.center
		init(center, level, texture, walk_anim, attack_anim, nb_anim_attack, dmg, spawn_tile)
				sprite.scale = 0.03125
		level.weapons.add self
	end
end
