import gamnit::virtual_gamepad

import dawn_arrival

redef class App
	redef fun on_create
	do
		super

		var gamepad = new VirtualGamepad
		gamepad.add_dpad(["up", "left", "down", "right"])
		gamepad.add_button("space", gamepad_spritesheet.fist)
		gamepad.visible = true
		self.gamepad = gamepad
	end
end
